import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';
import {random} from 'lodash';

export function updateSlider(name,value,oldSlider){
  const slider = Object.assign({},oldSlider,
    {
      [name]:value
    });
  return{
    type: types.ADD4_UPDATE_SLIDER,
    slider
  };
}

export function fetchDataSuccess(items,slider) {
    return {
        type: types.ADD4_FETCH_DATA_SUCCESS,
        items,
        slider
    };
}

export function updateGraph(slider) {
  const fv = `https://api.unpie.eu/wrapper.add4?mu=${slider.mu}&sigma=${slider.vol}
  &wealth=${slider.wealth}&nScenarios=10&prob=${slider.prob}&minumumRuinTime=${slider.mtr}&seed=${random(1,99999)}`;
  const func = fetchDataSuccess;
  const additionParams = [slider];
  return fectAndDispact(fv,func,additionParams);
}
