import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case3UpdateSlider(name, value, oldSlider) {
  const slider = Object.assign({}, oldSlider, {[name]: value});
  return {
    type: types.CASE3_UPDATE_SLIDER,
    slider
  };
}

export function case3PmtsFetchDataSuccess(items) {
    return {
        type: types.CASE3_PMTS_FETCH_DATA_SUCCESS,
        items
    };   
}

export function case3WithoutInterestFetchDataSuccess(items) {
  return {
      type: types.CASE3_WITHOUTINTEREST_FETCH_DATA_SUCCESS,
      items
  };
  
}

export function case3WithInterestFetchDataSuccess(items) {
    return {
        type: types.CASE3_WITHINTEREST_FETCH_DATA_SUCCESS,
        items
    };
}

export function case3UpdateGraph(slider) {
  const pmts = `https://api.unpie.eu/pmt.growth?pmtGrowth=${slider.growth}&nper=${slider.nper}&pmt=${slider.pmt}`;
  const fvWithInterest = `https://api.unpie.eu/pv.growth?pmtGrowth=${slider.growth}&rate=${slider.rate}&nper=${slider.nper}&pmt=-${slider.pmt}`;
  const fvWithOutInterest = `https://api.unpie.eu/case3.wrapper?pmtGrowth=${slider.growth}&nper=${slider.nper}&pmt=-${slider.pmt}&rate=${slider.rate}`;

  const funcpmts = case3PmtsFetchDataSuccess;
  const funcWithInterest = case3WithInterestFetchDataSuccess;
  const funcWithOutInterst = case3WithoutInterestFetchDataSuccess;
  
  return (dispatch) => {
    dispatch(fectAndDispact(pmts,funcpmts));
    dispatch(fectAndDispact(fvWithInterest,funcWithInterest));
    dispatch(fectAndDispact(fvWithOutInterest,funcWithOutInterst));

  };
}
