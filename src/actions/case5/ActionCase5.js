import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case5UpdateSlider(name, value, oldSlider) {
  const slider = Object.assign({}, oldSlider, {[name]: value});
  return {
    type: types.CASE5_UPDATE_SLIDER,
    slider
  };
}

export function case5PmtsFetchDataSuccess(items){
  return {
      type: types.CASE5_PMTS_FETCH_DATA_SUCCESS,
      items
  };
}

export function case5PvRealFetchDataSuccess(items){
  return {
      type: types.CASE5_PV_REAL_FETCH_DATA_SUCCESS,
      items
  };
}

export function case5FvNotAdjFetchDataSuccess(items){
  return {
      type: types.CASE5_FV_NOTADJINFLATION_FETCH_DATA_SUCCESS,
      items
  };
}

export function case5FvAdjFetchDataSuccess(items) {
    return {
        type: types.CASE5_FV_ADJINFLATION_FETCH_DATA_SUCCESS,
        items
    };
}

export function case5PmtWithInterestFetchDataSuccess(items) {
    return {
        type: types.CASE5_PMT_WITHINTEREST_FETCH_DATA_SUCCESS,
        items
    };
}

export function case5UpdateGraph(slider) {
  const pmts = `https://api.unpie.eu/pmt.growth?pmtGrowth=${slider.growth}&nper=${slider.nper}&pmt=${slider.pmt}&pmtinfladj=FALSE`;
  const wrapper5 =  `https://api.unpie.eu/case5.wrapper1?pmtGrowth=${slider.growth}&nper=${slider.nper}&pmt=-${slider.pmt}&rate=${slider.rate}&inflation=${slider.inflation}&pmtinfladj=TRUE`;
  const pvReal = `https://api.unpie.eu/case5.wrapper2?pmtGrowth=${slider.growth}&nper=${slider.nper}&pmt=-${slider.pmt}&rate=${slider.rate}&inflation=${slider.inflation}&pmtinfladj=TRUE`;

  const fv = `https://api.unpie.eu/case5.wrapper3?pmtGrowth=${slider.growth}&nper=${slider.nper}&pmt=-${slider.pmt}&rate=${slider.rate}&inflation=${slider.inflation}&pmtinfladj=TRUE`;
  const pmtAdj =  `https://api.unpie.eu/pmt.growth?pmtGrowth=${slider.growth}&nper=${slider.nper}&pmt=${slider.pmt}&inflation=${slider.inflation}&pmtinfladj=TRUE`;

  const funcpmts = case5PmtsFetchDataSuccess;
  const funcWrapper5 = case5FvNotAdjFetchDataSuccess;
  const funcPvReal = case5PvRealFetchDataSuccess;

  const funcAdjFetch = case5FvAdjFetchDataSuccess;
  const funcPmtAdj = case5PmtWithInterestFetchDataSuccess;

    return (dispatch) => {
      dispatch(fectAndDispact(pmts,funcpmts));
      dispatch(fectAndDispact(pvReal,funcPvReal));
      dispatch(fectAndDispact(wrapper5,funcWrapper5));
      dispatch(fectAndDispact(fv,funcAdjFetch));
      dispatch(fectAndDispact(pmtAdj,funcPmtAdj));
    };
}
