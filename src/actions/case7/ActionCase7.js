import * as types from '../../constants/actionTypes';
import {fectAndDispact} from '../ActionHelper';

export function case7UpdateSlider(name, value, oldSlider) {
  const slider = Object.assign({}, oldSlider, {[name]: value});
  return {
    type: types.CASE7_UPDATE_SLIDER,
    slider
  };
}

export function case7SpendingRequiredNotAdjFetchDataSuccess(items) {
  return {
      type: types.CASE7_SPENDING_REQUIRED_NOTADJINFLATION_FETCH_DATA_SUCCESS,
      items
  };
}

export function case7UpdateGraph(slider) {
  const pv = `https://api.unpie.eu/pvAnnuityGrowth?rate=${slider.rate}&nper=${slider.nper}&inflation=${slider.inflation}&netWorth=${-slider.networth}&annuityGrowth=${slider.growth}`;
 
  const funcPv = case7SpendingRequiredNotAdjFetchDataSuccess;

  return (dispatch) => {
    dispatch(fectAndDispact(pv,funcPv));
  };
}
