import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions/case3/ActionCase3';
import FutureValueSlider from './ValueSlider';
import { CustomizedBarChart } from '../ui/CustomizedBarChartInf';
import { array2BarData } from '../../utils/dataHelper';
import { color } from '../../color';
import intl from 'react-intl-universal';
import { ThousandFormatter } from '../ui/Formatter';
import { styles } from '../../styles';


export const FutureValueContainer = (props) => {

  const legend = [
    {
      value: intl.get('PERIODIC_SAVINGS_PAYMENT'),  
      type: 'square',
      color: color.primary
    }, {
      value: intl.get('SUM_OF_ALL_PERIODIC_SAVINGS'), 
      type: 'square',
      color: color.secondary
    }/*, {
      value: intl.get('FUTURE_VALUE'), 
      type: 'square',
      color: color.tertiary
    }*/
  ];
  
  let data = [];
  
data.push({"text": `0`, "value": props.case3.fvWithInterest[props.case3.fvWithInterest.length-1], "color":legend[1].color});

  let data2 = array2BarData(
    props.case3.pmts,
    legend[0].color
  );
  data.push(...data2);
  let data3 = [];
  data3.push({"text": props.case3.fvWithOutInterest.length, "value": props.case3.fvWithOutInterest[props.case3.fvWithOutInterest.length-1]});
  return (
    <div>
      <div>
        <div className="col-sm-3">
          <FutureValueSlider
            updateSlider={props.actions.case3UpdateSlider}
            updateGraph={props.actions.case3UpdateGraph}
            slider={props.case3.slider}
          />
        </div>

        <div className="col-sm-9">

          <h3 className="text-center">
            {intl.get('CASE3_TITLE')}
          </h3>
          <CustomizedBarChart
            data={data}
            legend={legend}
            legendLayout="horizontal"
            legendVerticalAlign="bottom"
            legendAlign="center"
          />

        <div style={styles.smallMarginTop} className={"col-sm-12 col-sm-offset-1"}>
          <h4>
            {intl.get('SUM_OF_ALL_PERIODIC_SAVINGS')}: <mark>{ThousandFormatter(data[0].value)}</mark>
            <br />
            <br />
            {intl.get('FUTURE_VALUE')}: <mark>{ThousandFormatter(data3[data3.length-1].value)}</mark>
            </h4>
          </div>

        </div>
      </div>

    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case3: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return { case3: state.case3 };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
