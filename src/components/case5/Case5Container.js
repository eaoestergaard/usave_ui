import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case5/ActionCase5';
import FutureValueSlider from './ValueSlider';
import {CustomizedBarChart} from '../ui/CustomizedBarChartInf';
import {array2BarData} from '../../utils/dataHelper';
import {color} from '../../color';
import intl from 'react-intl-universal';
import { ThousandFormatter } from '../ui/Formatter';
import { styles } from '../../styles';
import {sortBy} from 'lodash';

export const FutureValueContainer = (props) => {
  const legend = [
    {
      value: intl.get('PERIODIC_SAVINGS_PAYMENT_REAL'), 
      type: 'square',
      color: color.primary
    },
    {
      value: intl.get('PERIODIC_SAVINGS_PAYMENT_NOMINAL'), 
      type: 'square',
      color: color.secondary
    },
    /*{ 
      value: intl.get('FUTURE_VALUE_NOMINAL'),  
      type: 'square',
      color: color.quaternary
    },*/      
    {
      value: intl.get('SUM_OF_ALL_PERIODIC_SAVINGS_NOMINAL'),   
      type: 'square',
      color: color.quinary
    },
    {
      value: intl.get('FUTURE_VALUE_REAL'),       
      type: 'square',
      color: color.tertiary
    }
  ];
  {/* Merge data into barChart data */}
  //
  let data = [];

data.push({"text": 0, "value": props.case5.wrapper5[props.case5.wrapper5.length-1], "color":legend[2].color});
data.push({"text": 0, "value": props.case5.PvReal[props.case5.PvReal.length-1], "color":legend[3].color});
const data1 = array2BarData(props.case5.pmts, legend[0]['color']);
const data2 = array2BarData(props.case5.pmtAdjInf, legend[1]['color']);
let data3 = [];
data3.push(...data1,...data2);
data3 = sortBy(data3, [function(x) {
  return parseInt(x.text, 10);
  }
]); 
data.push(...data3);
let data4 = [];
data4.push({"text": '0', "value": props.case5.fv[props.case5.fv.length-1]});


  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case5UpdateSlider}
          updateGraph={props.actions.case5UpdateGraph}
          slider={props.case5.slider}
          />
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('CASE5_TITLE')}
        </h3>
        <CustomizedBarChart
          data={data}
          legend={legend}
          legendLayout="horizontal"
          legendVerticalAlign="bottom"
          legendAlign="center"
          />

          <div style={styles.smallMarginTop} className={"col-sm-12 col-sm-offset-1"}>
          <h4>
            {intl.get('SUM_OF_ALL_PERIODIC_SAVINGS_NOMINAL')}: <mark>{ThousandFormatter(data[0].value)}</mark>
            <br />
            <br />
            {intl.get('FUTURE_VALUE_REAL')}: <mark>{ThousandFormatter(data[1].value)}</mark>
            <br />
            <br />
                        
          </h4>
          </div>

      </div>


    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case5: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case5: state.case5};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
