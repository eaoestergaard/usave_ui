import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../actions/case6/ActionCase6';
import FutureValueSlider from './ValueSlider';
import {CustomizedBarChart} from '../ui/CustomizedBarChartInf';
import {generateRepeatBarData} from '../../utils/dataHelper';
import {color} from '../../color';
import intl from 'react-intl-universal';
import { ThousandFormatter } from '../ui/Formatter';
import { styles } from '../../styles';


export const FutureValueContainer = (props) => {
  const legend = [
    {
      value: intl.get('PERIODIC_SPENDING'), //'Spending per year',
      type: 'square',
      color: color.primary
    }, {
      value: intl.get('SUM_OF_PERIODIC_SPENDING'), //'Total Spending during retirement (w/o interest)',
      type: 'square',
      color: color.secondary
    }, {
      value: intl.get('REQUIRED_WEALTH_TO_FINANCE_PERIODIC_SPENDING'), //'Wealth required to finance total spending during retirement',
      type: 'square',
      color: color.tertiary
    }
  ];


  const data = [...props.case6.barChart];
  const repeatData = generateRepeatBarData(props.case6.slider.nper + 1, props.case6.slider.pmt, legend[0].color);
  data.push(...repeatData);

  return (
    <div>
      <div className="col-sm-3">
        <FutureValueSlider
          updateSlider={props.actions.case6UpdateSlider}
          updateGraph={props.actions.case6UpdateGraph}
          slider={props.case6.slider} />
      </div>
      <div className="col-sm-9">
        <h3 className="text-center">
          {intl.get('CASE6_TITLE')}
        </h3>
        <CustomizedBarChart
          data={data}
          legend={legend}
          legendLayout="horizontal"
          legendVerticalAlign="bottom"
          legendAlign="center"
          />

          <div style={styles.smallMarginTop} className={"col-sm-12 col-sm-offset-1"}>
          <h4>
            {intl.get('SUM_OF_PERIODIC_SPENDING')}: <mark>{ThousandFormatter(data[0].value)}</mark>
            <br />
            <br />
            {intl.get('REQUIRED_WEALTH_TO_FINANCE_PERIODIC_SPENDING')}: <mark>{ThousandFormatter(data[1].value)}</mark>
            </h4>
          </div>

      </div>
    </div>
  );
};

FutureValueContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  case6: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {case6: state.case6};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FutureValueContainer);
