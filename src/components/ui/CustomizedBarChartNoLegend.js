import React from 'react';
import PropTypes from 'prop-types';
import {
  XAxis,
  YAxis,
  CartesianGrid,
  Bar,
  BarChart,
  Cell,
  Label,
  ResponsiveContainer
} from 'recharts';
import {CustomizedLabel} from './CustomizedLabel';
import {ThousandFormatter} from './Formatter';



export function CustomizedBarChartNoLegend(props) {
  const {data} = props;
  return (
    <ResponsiveContainer width={'100%'} height={360}>
    <BarChart data={data} margin={{
      top: 60,
      right: 0,
      left: 20,
      bottom: 7
    }}>
    <CartesianGrid vertical={false} stroke="#ebf3f0"/>
      <XAxis dataKey="text" fontFamily="sans-serif" dy={10}>
      <Label value="T" offset={-5} dx={15} position="insideTopRight" />
      </XAxis>
      <YAxis tickFormatter={ThousandFormatter} />

      <Bar dataKey="value" barSize ={170} label={<CustomizedLabel />}>
        {data.map((entry, index) => (<Cell key={index} fill={data[index].color}/>))
}
      </Bar>
    </BarChart>
    </ResponsiveContainer>
  );
}

CustomizedBarChartNoLegend.propTypes = {
  data: PropTypes.array.isRequired,
  legend: PropTypes.array
};
