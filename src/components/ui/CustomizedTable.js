import React from 'react';
import PropTypes from 'prop-types';
import {ThousandFormatter2Int} from './Formatter';
import {styles} from '../../styles';
//import Radium from "radium";

class CustomizedTable extends React.Component {
    render() {
    const list1 = this.props.List1.map((number, index)=>
      <td style={styles.table2.td} key={index}>{ThousandFormatter2Int(number)}</td>
    );

    const list2 = this.props.List2.map((number, index)=>
      <td style={styles.table2.td} key={index}>{ThousandFormatter2Int(number)}</td>
    );
    if (this.props.List1 && this.props.List1.length){
    return (
      <table style={styles.table2}>
        <tbody>
        <tr style={styles.table2.tr} key={1}>
          <th style={styles.table2.th}>{this.props.Label1}:</th>
          {list1}
        </tr>
        <tr style={styles.table2.tr} key={2}>
          <th style={styles.table2.th}>{this.props.Label2}:</th>
          {list2}
        </tr>
        </tbody>
      </table>
    );
  }
  return(<div/>);
}
  
}

//CustomizedTable = Radium(CustomizedTable);

CustomizedTable.propTypes = {
  Label1: PropTypes.string.isRequired,
  Label2: PropTypes.string.isRequired,
  List1: PropTypes.array.isRequired,
  List2: PropTypes.array.isRequired
};

export default CustomizedTable;
