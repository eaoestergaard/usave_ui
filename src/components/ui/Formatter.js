import {round} from 'lodash';
export function ThousandFormatter(tickItem) {
  let value = round(tickItem,2);
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "‘");
}

export function ThousandFormatter2Int(tickItem) {
  let value = round(tickItem);
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "‘");
}