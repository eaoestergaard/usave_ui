import * as types from '../constants/actionTypes';
import initialState from './initialState';
import {mapArrayObjAdd3} from './ReducerHelpers';

export function add1(state = initialState.add1, action) {
  switch (action.type) {
    case types.ADD1_UPDATE_SLIDER:
      {
        return {
          ...state,
          slider: action.slider
        };
      }
    case types.ADD1_FETCH_DATA_SUCCESS:
      {
        const LifeLongSorted = action.items.Lifelong_pension_sorted;
        const FutureValueSorted = action.items.Future_value_sorted;
        const Scenarios = mapArrayObjAdd3(action.items.Scenarios);
        
        return {
          ...state,
          LifeLongSorted,
          FutureValueSorted,
          Scenarios,
          slider: action.slider
        };
      }
    default:
      return state;
  }
}
