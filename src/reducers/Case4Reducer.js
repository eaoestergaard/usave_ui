import * as types from '../constants/actionTypes';
import initialState from './initialState';
import {updateObjectInArray} from './ReducerHelpers';
import {sum,round} from 'lodash';

export function case4(state = initialState.case4, action) {
  switch (action.type) {
    case types.CASE4_UPDATE_SLIDER:
    {
      return {
        ...state,
        slider: action.slider
      };
    }
    
    case types.CASE4_PMTS_FETCH_DATA_SUCCESS:
      {
        const item = {
          index: 2,
          item: {
            "text": `${action.items.length}`,
            "value": round(sum(action.items),4)
          }
        };
        const barChart = updateObjectInArray(state.barChart, item);
        return {
          ...state,
          barChart,
          pmts: action.items
        };
      }

      case types.CASE4_ADJINF_FETCH_DATA_SUCCESS:
      {
        const item = {
          index: 2,
          item: {
            "text": `${action.items.length}`,
            "value": action.items[action.items.length-1]
          }
        };
        const barChart = updateObjectInArray(state.barChart, item);
        return {
          ...state,
          barChart,
          fvAdj: action.items
        };
      }


    case types.CASE4_NOTADJINF_FETCH_DATA_SUCCESS:
      {
        const item = {
          index: 1,
          item: {
            "text": `${action.items.length}`,
            "value": action.items[action.items.length-1],
          }
        };
        const barChart = updateObjectInArray(state.barChart, item);
        return {
          ...state,
          barChart,
          fvNotAdj: action.items
        };
      }

    case types.CASE4_WITHOUTINTEREST_FETCH_DATA_SUCCESS:
      {
        const item = {
          index: 0,
          item: {
            "text": `${action.items.length}`,
            "value": action.items[action.items.length-1]
          }
        };
        const barChart = updateObjectInArray(state.barChart, item);
        return {
          ...state,
          barChart,
          fvSum: action.items

        };
      }
    default:
      return state;
  }
}
