import * as types from '../constants/actionTypes';
import initialState from './initialState';
import {updateObjectInArray} from './ReducerHelpers';
import {sum,round} from 'lodash';

export function case5(state = initialState.case5, action) {
  switch (action.type) {
    case types.CASE5_UPDATE_SLIDER:
    {
      return {
        ...state,
        slider: action.slider
      };
    }
    case types.CASE5_PMTS_FETCH_DATA_SUCCESS:
      {
        const item = {
          index: 2,
          item: {
            "text": `${action.items.length}`,
            "value": round(sum(action.items),4)
          }
        };
        const barChart = updateObjectInArray(state.barChart, item);
        return {
          ...state,
          barChart,
          pmts: action.items
        };
      }
      case types.CASE5_PV_REAL_FETCH_DATA_SUCCESS:
        {
          const item = {
            index: 1,
            item: {
              "text": `0`,
              "value": action.items[0]
            }
          };
          const barChart = updateObjectInArray(state.barChart, item);
          return {
            ...state,
            barChart,
            PvReal: action.items
          };
        }
    case types.CASE5_FV_NOTADJINFLATION_FETCH_DATA_SUCCESS:
      {
        const item = {
          index: 1,
          item: {
            "text": `${action.items.length}`,
            "value": action.items[action.items.length-1]
          }
        };
        const barChart = updateObjectInArray(state.barChart, item);
        return {
          ...state,
          barChart,
          wrapper5: action.items
        };
      }

     case types.CASE5_FV_ADJINFLATION_FETCH_DATA_SUCCESS:
      {
        const item = {
          index: 2,
          item: {
            "text": 0,
            "value": action.items[action.items.length-1]
          }
        };
        const barChart = updateObjectInArray(state.barChart, item);
        return {
          ...state,
          barChart,
          fv: action.items
        };
      }

    case types.CASE5_PMT_WITHINTEREST_FETCH_DATA_SUCCESS:
      {
        const item = {
          index: 0,
          item: {
            "text": 0,
            "value": round(sum(action.items),4)
          }
        };
        const barChart = updateObjectInArray(state.barChart, item);
        return {
          ...state,
          barChart,
          pmtAdjInf: action.items
        };
      }
    default:
      return state;
  }
}
