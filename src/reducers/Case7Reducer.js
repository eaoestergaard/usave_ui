import * as types from '../constants/actionTypes';
import initialState from './initialState';

export function case7(state = initialState.case7, action) {
  switch (action.type) {
    case types.CASE7_UPDATE_SLIDER:
    {
      return {
        ...state,
        slider: action.slider
      };
    }

    case types.CASE7_SPENDING_REQUIRED_NOTADJINFLATION_FETCH_DATA_SUCCESS:{
        return {
          ...state,
          spendingAdjInf:action.items
        };
    }

    default:
      return state;
  }
}
