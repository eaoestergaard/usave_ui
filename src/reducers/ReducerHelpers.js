export function updateObjectInArray(array, action) {
  return array.map((item, index) => {
    if (index !== action.index) {
      // This isn't the item we care about - keep it as-is
      return item;
    }

    // Otherwise, this is the one we want - return an updated value
    return {
      ...item,
      ...action.item
    };
  });
}

export function insertItem(array, action) {
  return [
    ...array.slice(0, action.index),
    action.item,
    ...array.slice(action.index)
  ];
}

export function removeItem(array, action) {
  return [
    ...array.slice(0, action.index),
    ...array.slice(action.index + 1)
  ];
}

export function generateRepeatBarChartData(ntimes, value) {
  let data = [];
  let i;
  for (i = 1; i < ntimes; i++) {
    data.push(
      {
        "Text": `t=${i}`,
        "value": value
      });
  }
  return data;
}

export function mapArrayIntoBarChartData(array, color) {
  let data = [];
  array.map((entry, index) => data.push(
    {
      "Text": `t=${index}`,
      "value": entry,
      color
    }));
  return data;
}

export function mapArrayIntoBarChartDataAdd1(array) {
  let data = [];
  array.map((entry, index) => data.push(
    {
      "text": index+1,
      "value": entry
    }));
  return data;
}

// Only used in transform R data into plotting object
// cause by inconsistence in backend, current quick fix, 
// when there is time, fix it in backend.
export function mapArrayObjAdd2(array){
  let data = [];
  let nRow = array.length;
  let nCol = array[0].length;
  for(let i=0; i<nCol; i++){
    let innerObj = {T:`${i+1}`};
    for(let j=0; j<nRow; j++) {
     innerObj[j+1]=array[j][i];
    }
   data.push(innerObj);
  }
  return data;
}

// Only used in timeToRuin.scenario to transform R data into plotting object
export function mapArrayObjAdd3(array){
  let data = [];
    for(let i=0; i<array.length; i++){
    let iReakke = array[i].length;
    let innerObj = {T:`${i+1}`};
    for(let j=0; j<iReakke; j++) {
      innerObj[j+1]=array[i][j];
   }
   data.push(innerObj);
  }
  return data;
}

// Only used in maximumSpendingForMinimumRuinTime to transform data into plotting object
// DataStructure changed again!!
export function mapArrayObjAdd4(array){
  let temp=array[0].map((col, i) => array.map(row => row[i]));
  return mapArrayObjAdd3(temp);
}
