import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import {itemsHasErrored,itemsIsLoading} from './GeneralReducer';
import {case1} from './Case1Reducer';
import {case2} from './Case2Reducer';
import {case3} from './Case3Reducer';
import {case4} from './Case4Reducer';
import {case5} from './Case5Reducer';
import {case6} from './Case6Reducer';
import {case7} from './Case7Reducer';
import {case8} from './Case8Reducer';
import {case9} from './Case9Reducer';
import {add1} from './Add1Reducer';
import {add2} from './Add2Reducer';
import {add3} from './Add3Reducer';
import {add4} from './Add4Reducer';

const rootReducer = combineReducers({
  case1,
  case2,
  case3,
  case4,
  case5,
  case6,
  case7,
  case8,
  case9,
  add1,
  add2,
  add3,
  add4,
  itemsHasErrored,
  itemsIsLoading,
  routing: routerReducer
});

export default rootReducer;
