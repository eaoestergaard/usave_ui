import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';

import case1 from './components/case1/Case1Container';
import case2 from './components/case2/Case2Container';
import case3 from './components/case3/Case3Container';
import case4 from './components/case4/Case4Container';
import case5 from './components/case5/Case5Container';
import case7 from './components/case7/Case7Container';
import AboutPage from './components/AboutPage';
import NotFoundPage from './components/NotFoundPage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={case1}/>
    <Route path="case1" component={case1}/>
    <Route path="case2" component={case2}/>
    <Route path="case3" component={case3}/>
    <Route path="case4" component={case4}/>
    <Route path="case5" component={case5}/>
    <Route path="case6" component={case7}/>
    <Route path="about" component={AboutPage}/>
    <Route path="*" component={NotFoundPage}/>
  </Route>
);
