export function padLeadingZero(value) {
  return value > 9 ? value : `0${value}`;
}

export function generateRepeatBarData(ntimes, value, color) {
  let data = [];
  let i;
  for (i = 1; i < ntimes; i++) {
    data.push({"text": `${i}`, "value": value, "color":color});
  }
  return data;
}

export function array2BarData(array, color){
  let data = [];
  array.map(function(x,i){
    data.push({"text": `${i+1}`, "value": x, "color":color});
  });
  return data;
}

export function updateObjectInArray(array, action) {
  return array.map((item, index) => {
    if (index !== action.index) {
      // This isn't the item we care about - keep it as-is
      return item;
    }

    // Otherwise, this is the one we want - return an updated value
    return {
      ...item,
      ...action.item
    };
  });
}
